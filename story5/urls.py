from django.urls import path
from . import views

urlpatterns = [
    path('', views.matkul_list, name='matkul_list'),
    path('matkul/<int:id>/', views.matkul_page, name='matkul_page'),
    path('add', views.matkul_add, name='matkul_add'),
    path('matkul/<int:id>/delete', views.matkul_delete, name='matkul_delete')
]