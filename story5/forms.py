from django import forms
from .models import MataKuliah

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        # widgets = {
        #     'nama' : forms.TextInput(label='Nama Mata Kuliah', max_length=50),
        #     'dosen' : forms.TextInput(label='Nama Dosen', max_length=50),
        #     'jumlah_sks' : forms.TextInput(label='Jumlah SKS'),
        #     'semester_tahun' : forms.TextInput(label='Semester Tahun', max_length=20),
        #     'ruang_kelas' : forms.TextInput(label='Ruang Kelas', max_length=10),
        #     'deskripsi' : forms.TextInput(label='Deskripsi'),
        # }
        fields = ('nama', 'dosen', 'jumlah_sks', 'semester_tahun', 'ruang_kelas', 'deskripsi')
