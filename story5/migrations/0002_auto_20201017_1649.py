# Generated by Django 2.1.1 on 2020-10-17 09:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='matakuliah',
            old_name='jumlahsks',
            new_name='jumlah_sks',
        ),
        migrations.RenameField(
            model_name='matakuliah',
            old_name='ruangkelas',
            new_name='ruang_kelas',
        ),
        migrations.RenameField(
            model_name='matakuliah',
            old_name='semesterthn',
            new_name='semester_tahun',
        ),
    ]
