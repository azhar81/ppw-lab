# Generated by Django 3.1.2 on 2020-10-17 12:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0003_auto_20201017_1932'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matakuliah',
            name='dosen',
            field=models.CharField(help_text='Nama Dosen', max_length=50),
        ),
        migrations.AlterField(
            model_name='matakuliah',
            name='nama',
            field=models.CharField(help_text='Nama mata kuliah', max_length=50),
        ),
    ]
