from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama = models.CharField(max_length=50, help_text='Nama mata kuliah')
    dosen = models.CharField(max_length=50, help_text='Nama Dosen')
    jumlah_sks = models.IntegerField(help_text='Jumlah SKS')
    deskripsi = models.TextField(help_text='Deskripsi')
    semester_tahun = models.CharField(max_length=20, help_text='Semester tahun (contoh: Gasal 2020/2021)')
    ruang_kelas = models.CharField(max_length=10, help_text='Ruang Kelas')

    def __str__(self):
        return self.nama

# MataKuliah(nama='MatDas3', dosen='Azhar', jumlah_sks=21, deskripsi='none', semester_tahun='Gasal 2020/2021', ruang_kelas='A202')