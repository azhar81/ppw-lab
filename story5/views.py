from django.shortcuts import render, get_object_or_404, redirect
from .models import MataKuliah
from .forms import MatkulForm

# Create your views here.
def matkul_list(request):
    matkuls = MataKuliah.objects.all()
    return render(request, 'story5/landing.html', {'matkuls':matkuls})

def matkul_page(request, id):
    matkul = get_object_or_404(MataKuliah, id=id)
    return render(request, 'story5/view.html', {'matkul':matkul})

def matkul_add(request):
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            matkul = form.save()
            matkul.save()
            return redirect('matkul_page', id=matkul.id)
    else:
        form = MatkulForm()
    return render(request, 'story5/form.html', {'form':form})

def matkul_delete(request, id):
    MataKuliah.objects.get(id=id).delete()
    return redirect('matkul_list')