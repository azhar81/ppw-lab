from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'lab_3.html')

def other(request):
    return render(request, 'other.html')