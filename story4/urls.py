from django.urls import re_path
from .views import index, other
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path('other', other)
]
