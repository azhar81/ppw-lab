from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Azhar Hassanuddin'
npm = 1806235763

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'npm': npm}
    return render(request, 'index_lab1.html', response)
